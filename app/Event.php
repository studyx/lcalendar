<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use MaddHatter\LaravelFullcalendar\Event as CalendarEvent;

class Event extends Model implements CalendarEvent
{
    protected $table = 'events';

    protected $dates = ['begin', 'end'];

    protected $fillable = ['name', 'begin', 'end', 'fullday', 'license_id', 'customer_id', 'instructor_id', 'location_id'];

    // color
    // bgcolor

    public function license()
    {
        $this->belongsTo('App\License');
    }

    public function customer()
    {
        $this->belongsTo('App\Customer');
    }

    public function instructor()
    {
        $this->belongsTo('App\Instructor');
    }

    public function location()
    {
        $this->belongsTo('App\Location');
    }

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->name;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->fullday;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->begin;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function getEventOptions()
    {
        return [
            'color' => $this->color,
            'firstDay' => 1,
            'weekends' => false,
            'weekNumbers' => true,
        ];
    }

}
