<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = ['name', 'email', 'age'];

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
