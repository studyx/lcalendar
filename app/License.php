<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $table = 'licenses';

    protected $fillable = ['name'];

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
