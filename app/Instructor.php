<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    protected $table = 'instructors';

    protected $fillable = ['name', 'email', 'telephone'];

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
