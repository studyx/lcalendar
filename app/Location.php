<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';

    protected $fillable = ['street', 'streetnumber', 'postalcode', 'city', 'country'];

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
