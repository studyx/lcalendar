<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('begin');
            $table->timestamp('end');
            $table->timestamps();

            $table->integer('license_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->integer('instructor_id')->unsigned();

            // Licenses
            $table  ->foreign('license_id')
                    ->references('id')
                    ->on('licenses')
                    ->onDelete('cascade');

            // Customers
            $table  ->foreign('customer_id')
                    ->references('id')
                    ->on('customers')
                    ->onDelete('cascade');

            // Locations
            $table  ->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade');

            // Instructors
            $table  ->foreign('instructor_id')
                    ->references('id')
                    ->on('instructors')
                    ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
