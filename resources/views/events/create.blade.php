@extends('app')

@section('content')
    <h1>Maak een event aan</h1>

    {!! Form::open(['url' => 'admin/events']) !!}

        @include('events._form', ['buttontext' => 'Event aanmaken'])

    {!! Form::close() !!}

@stop