@extends('app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Events <small>Overview</small>
            </h1>
        </div>
    </div>
    <!-- /.row -->

    {!! $calendar->calendar() !!}
    {!! $calendar->script() !!}

@stop