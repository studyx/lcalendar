<div class="form-group">
    {!! Form::label('name', 'Naam:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('begin', 'Start:') !!}
    {!! Form::input('datetime', 'begin', date('Y-m-d H:i:s'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('end', 'Einde:') !!}
    {!! Form::input('datetime', 'end', date('Y-m-d H:i:s'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('fullday', 'Volledige dag:') !!}
    {!! Form::checkbox('fullday',1, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('license_id', 'Rijbewijs:') !!}
    {!! Form::select('license_id', $licenses, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('customer_id', 'Klant:') !!}
    {!! Form::select('customer_id', $customers, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instructor_id', 'Instructeur:') !!}
    {!! Form::select('instructor_id', $customers, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('location_id', 'Location:') !!}
    {!! Form::select('location_id', $locations, null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::submit($buttontext, ['class' => 'btn btn-primary form-control']) !!}
</div>