var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.sass('app.scss');

    mix.styles([
        'vendor/bootstrap/bootstrap.min.css',
        'vendor/fullcalendar/fullcalendar.min.css',
        'vendor/fullcalendar/fullcalendar.print.css',
        'vendor/font-awesome/font-awesome.min.css',
        'vendor/sb-admin-2/sb-admin-2.min.css',
        'vendor/datatables-plugins/dataTables.bootstrap.css',
        'vendor/datatables-responsive/dataTables.responsive.css',
        'app.css'
    ]);

    mix.copy('resources/assets/fonts', 'public/fonts');

    mix.scripts([
        'vendor/jquery.min.js',
        'vendor/bootstrap/bootstrap.min.js',
        'vendor/fullcalendar/moment.min.js',
        'vendor/fullcalendar/fullcalendar.min.js',
        'vendor/sb-admin-2/sb-admin-2.min.js',
        'vendor/metisMenu/metisMenu.min.js',
        'vendor/datatables/jquery.dataTables.min.js',
        'vendor/datatables-plugins/dataTables.bootstrap.min.js',
        'vendor/datatables-responsive/dataTables.responsive.js',
        'app.js'
    ]);

});
